<?php 
if (!defined('ABSPATH')) {
    exit;
}
 ?>

<div class="container">
    <h2>
        <?php esc_html_e('Setting', 'jobs-board');?>
    </h2>
</div>
<div class="container tab-general-details-jp-contact-form" id="general_jp_contact_form_tab" style="margin-top:20px;">
    <ul class="nav nav-pills">
        <li class="active">
            <a data-toggle="tab" href="#1a">
                <?php echo esc_html__('General details', 'jp-contact-form'); ?>
            </a>
        </li> 
        
        <li>
            <a data-toggle="tab" href="#3a">
                <?php echo esc_html__('How can use', 'jp-contact-form'); ?>
            </a>
        </li>
        
    </ul>
    <div class="tab-content clearfix">
         <div class="tab-pane active" id="1a">
            
    <div class="wrap">

    	<?php   $admin_email = ( FALSE !== get_option( 'settings_admin_email' ) ) ? get_option( 'settings_admin_email' ) : get_option( 'admin_email' );

    	 $options = get_option('admin_email_enabled_nottifications');
    	 echo "          
        <h1> Email Notifications </h1>
        <div>
            <form action='options.php' method='post' name='options'>
            " . wp_nonce_field('update-options') . "";

            ?>
              <h6>Admin email : <?php echo  $admin_email; ?> </h6>
            <div class="form_field_part">
           
            	<label>

            	<input type="checkbox" name="admin_email_enabled_nottifications" value="1"
             <?php if ( 1 == $options ) echo 'checked="checked"'; ?> />
             Enabled Admin email nottifications for contact form
             </label>

            	
            </div>
 
                <input type='hidden' name='action' value='update' />
                <input type='hidden' name='page_options' value='admin_email_enabled_nottifications' />     
                <input type='submit' name='Submit' class="button button-primary" value='Update' />
            </form>
        </div>
   
       
    </div> 
        </div> 
        
 
               <div class="tab-pane" id="3a">
            <div class="jplf-section-group">
        
        <div class="col-md-12">
        	<p>Contact form shortcodes are a powerful feature to do cool stuff with little effort. You can do pretty much anything with them. With shortcodes for front end pages.</p>

        	<p>If you want to using shortcode for contact form, you have to simply type in the following shortcode:</p>
            <div class="jplf-preview-url">
                <?php echo esc_html__('[contact_form_shortcode]', 'jobs-board'); ?>
            </div>
            <p>If you want to contact form, you have to simply type in the following Function code:</p>
            <div class="jplf-preview-url">
            <?php echo esc_html__('jpcontact_form_shortcode(); 
                ', 'jobs-board'); ?>
            </div>
        </div>
    </div>
        </div>
        
    </div>
</div>
