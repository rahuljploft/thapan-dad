<?php 
if (!defined('ABSPATH')) {
    exit;
}

add_shortcode( 'contact_form_shortcode', 'jpcontact_form_shortcode' );

function jpcontact_form_func_for_shortcode( $args ) {
  echo '<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
';
echo '<style>.error{ color:red;} .success{ color : green;}</style>';
   wp_enqueue_script('custom-ajax-js', plugins_url('../assets/js/custom-ajax.js', __FILE__), array('jquery'));

    echo '<form action="' . esc_url( $_SERVER['REQUEST_URI'] ) . '" method="post" id="jpcontactform">';
	echo '<p>';
	echo 'Your Frist Name (required) <br/>';
	echo '<input type="text" name="first_name" pattern="[a-zA-Z0-9 ]+" class="form-control" required  />';
	echo '</p>';
		echo '<p>';
	echo 'Your Last Name  <br/>';
	echo '<input type="text" name="last_name" pattern="[a-zA-Z0-9 ]+" class="form-control"  />';
	echo '</p>';
	echo '<p>';
	echo 'Your Email (required) <br/>';
	echo '<input type="email" name="email" class="form-control" required />';
	echo '</p>';
		echo '<p>';
	echo 'Your contact number (required) <br/>';
	echo '<input type="tel" name="contact_no"  class="form-control"  required />';
	echo '</p>';
	echo '<p>';
	echo 'Subject (required) <br/>';
	echo '<input type="text" name="subject" pattern="[a-zA-Z ]+"  class="form-control" required />';
	echo '</p>';
	echo '<p>';
	echo 'Your Message (required) <br/>';
	echo '<textarea rows="10" cols="35" name="message" class="form-control"></textarea>';
	echo '</p>';
	echo '<p><input type="submit" name="submitted" value="Send"></p>';
	echo '</form>';
  

 

  
}



function admin_deliver_mail() {


       global $wpdb;

	// if the submit button is clicked, send the email
	if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['submitted']) ) {



		// sanitize form values
		$fname    = sanitize_text_field( $_POST["first_name"] );
		$lname    = sanitize_text_field( $_POST["last_name"] );
		$contact_no    = sanitize_text_field( $_POST["contact_no"] );
		$email   = sanitize_email( $_POST["email"] );
		$subject = sanitize_text_field( $_POST["subject"] );
		$message = esc_textarea( $_POST["message"] );

		$table_name = $wpdb->prefix . 'jpcontact_form_details';


		$results = $wpdb->insert($table_name, array('first_name' => $fname, 
        'last_name' => $lname,
        'email' => $email, 
        'contact_no' => $contact_no,
        'subject' => $subject, 
        'message' => $message));

        if($results){

        	echo '<div class="success">';
			echo '<p>Thanks for contacting me, expect a response soon.</p>';
			echo '</div>';

        // get the blog administrator's email address
		$to = get_option( 'admin_email' );

		$email_enabled = get_option('admin_email_enabled_nottifications');

		$headers = "From: $name <$email>" . "\r\n";

		// If email has been process for sending, display a success message
		if ($email_enabled =='1') {
			wp_mail( $to, $subject, $message, $headers );
		} 

        }else{
        	echo 'An unexpected error occurred';
        }

		
	}
}

function jpcontact_form_shortcode() {
	ob_start();
	admin_deliver_mail();
	jpcontact_form_func_for_shortcode($args);
	return ob_get_clean();
}


 ?>