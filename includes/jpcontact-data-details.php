<?php 
if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );




class My_contactform_List_Table extends WP_List_Table {

/** Class constructor */
public function __construct() {

parent::__construct( [
'singular' => __( 'Contact user', 'jp-contact-form' ), //singular name of the listed records
'plural' => __( 'Contact user', 'jp-contact-form' ), //plural name of the listed records
'ajax' => false //should this table support ajax?

] );

}

public static function get_applicants($per_page = 10, $page_number = 1 ) {

global $wpdb;


 $sql = "SELECT * FROM {$wpdb->prefix}jpcontact_form_details";

if ( ! empty( $_REQUEST['orderby'] ) ) {
$sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
$sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
}

$sql .= " LIMIT $per_page";

$sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;

    $result = $wpdb->get_results( $sql, 'ARRAY_A' );

    

    return $result;

}

function get_columns(){
  $columns = array(
    'cb' => '<input type="checkbox" />', 
        'first_name' => __('First Name', 'jp-contact-form') , 
        'last_name' => __('Last Name', 'jp-contact-form') , 
        'email' => __('Email address', 'jp-contact-form') , 
        'contact_no' => __('phone', 'jp-contact-form') , 
        'subject' => __('subject', 'jp-contact-form') , 
        'message' => __('message', 'jp-contact-form') , 
        'created_at' => __('Date', 'jp-contact-form') 
  );
  return $columns;
}

function prepare_items() {
  $columns = $this->get_columns();
  $hidden = array();
  $sortable = array();
  
$data = $this->get_applicants();
usort( $data, array( &$this, 'sort_data' ) );

$perPage = 10;
$currentPage = $this->get_pagenum();
$totalItems = count($data);

$this->set_pagination_args( array(
    'total_items' => $totalItems,
    'per_page'    => $perPage
) );

$data = array_slice($data,(($currentPage-1)*$perPage),$perPage);

$this->_column_headers = array($columns, $hidden, $sortable);
$this->items = $data;
}

function column_default( $item, $column_name ) {
  switch( $column_name ) { 
    case 'id':
    case 'first_name':
    case 'last_name':
     case 'email':
     case 'contact_no':
      case 'subject':
      case 'message':
      case 'created_at':
      return $item[ $column_name ];
    default:
      return print_r( $item, true ) ; //Show the whole array for troubleshooting purposes
  }
}
}



}