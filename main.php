<?php 
/**
 * @wordpress-plugin
 * Plugin Name:       Jp Contact Form
 * Plugin URI:        https://jploft.in
 * Description:       create a simple contact us form add to data and send mail.
 * Version:           1.1.2
 * Author:            Lokendra Singh
 * Author URI:        https://jploft.in
 * License:           GPL
 * License URI:       https://jploft.in
 * Text Domain:       jobs-board
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 * */


if (!defined('ABSPATH')) {
    exit;
}

if (!defined('JPCONTACTFORM_PLUGIN_VERSION')) {
    define('JPCONTACTFORM_PLUGIN_VERSION', '1.1.2');
}

update_option('jpcontactform_version', JPCONTACTFORM_PLUGIN_VERSION);

/*main plugin directory path define */

define('JPCONTACTFORM_MAIN_FILE_PATH', __FILE__);

define('JPCONTACTFORM_FILE_URL', __FILE__);


/* Styles for jobs board plugin back-end */

function jpcontact_form_scripts()
{
    $plugin_url = plugin_dir_url(__FILE__);

    wp_enqueue_style('style', $plugin_url . "/assets/css/admin-style.css");
    wp_enqueue_style('fontawesome-iconpicker-min', plugins_url('/assets/css/fontawesome-iconpicker.min.css', __FILE__));

    if ($_GET['page'] == 'jp_contact_form_page' || $_GET['page'] == 'jp_contact_data_details') {
        wp_enqueue_style('bootstrap-min-css', plugins_url('/assets/css/bootstrap.min.css', __FILE__));
        wp_enqueue_style('bootstrap-theme-min-css', plugins_url('/assets/css/bootstrap-theme.min.css', __FILE__));

        wp_enqueue_script('releated-script', plugins_url('/assets/js/jquery.min.js', __FILE__), array('jquery', 'jquery-ui-droppable', 'jquery-ui-draggable', 'jquery-ui-sortable'));

        wp_enqueue_script('releated-bootstrap-min', plugins_url('/assets/js/bootstrap.min.js', __FILE__), array('jquery'));
        wp_enqueue_script('bootstrap-bundle-min', plugins_url('/assets/js/bootstrap.bundle.min.js', __FILE__), array('jquery'));
       /* wp_enqueue_script('custom-modal-js', plugins_url('/assets/js/custom.js', __FILE__), array('jquery'));*/
    }


}

add_action('admin_print_styles', 'jpcontact_form_scripts');


/*front end js and css*/

function enqueue_short_related_pages_scripts_and_styles()
{
    wp_enqueue_style('related-styles', plugins_url('/assets/css/front-style.css', __FILE__));
    wp_enqueue_style('font-awesome-min', plugins_url('/assets/css/font-awesome.min.css', __FILE__));
    wp_enqueue_style('fontawesome-iconpicker-min', plugins_url('/assets/css/fontawesome-iconpicker.min.css', __FILE__));
    wp_enqueue_style('bootstrap-min-css', plugins_url('/assets/css/bootstrap.min.css', __FILE__));
    wp_enqueue_style('bootstrap-theme-min-css', plugins_url('/assets/css/bootstrap-theme.min.css', __FILE__));
    wp_enqueue_script('releated-script', plugins_url('/assets/js/jquery.min.js', __FILE__), array('jquery', 'jquery-ui-droppable', 'jquery-ui-draggable', 'jquery-ui-sortable'));

    wp_enqueue_script('releated-bootstrap-min', plugins_url('/assets/js/bootstrap.min.js', __FILE__), array('jquery'));
    wp_enqueue_script('bootstrap-bundle-min', plugins_url('/assets/js/bootstrap.bundle.min.js', __FILE__), array('jquery'));
    
      

}
add_action('wp_enqueue_scripts', 'enqueue_short_related_pages_scripts_and_styles');


/**
 * Register a jpcontactform menu page.
 */
function jpcontactform_menu_page() {
    
    add_menu_page(esc_html__('Jp contact form', 'jp-contact-form'), esc_html__('Jp contact form', 'jp-contact-form'), 'manage_options', 'jp_contact_form_page', 'jp_contact_form_page', 'dashicons-welcome-widgets-menus' );
    add_submenu_page('jp_contact_form_page', esc_html__('Contact details', 'jp-contact-form'), esc_html__('Contact details', 'jp-contact-form'), 'manage_options', 'jp_contact_data_details', 'jp_contact_data_details' );
    
}
add_action( 'admin_menu', 'jpcontactform_menu_page' );

function jp_contact_form_page(){

	require plugin_dir_path(__FILE__) . 'includes/jpcontact-genral-setting.php';


}

function jp_contact_data_details(){

$contactdetails = new My_contactform_List_Table();
  echo '<div class="wrap"><h2>Contact users List</h2>'; 
  $contactdetails->prepare_items(); 
  $contactdetails->display(); 
  echo '</div>';	

}


require plugin_dir_path(__FILE__) . 'includes/jpcontact-data-details.php';

require_once plugin_dir_path(__FILE__) . 'includes/jp-contact-form-shortcode.php';



function jpcontact_form_create_db() {
 global $wpdb;
 $charset_collate = $wpdb->get_charset_collate();
 require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

 //* Create the teams table
 $table_name = $wpdb->prefix . 'jpcontact_form_details';
 if ($wpdb->get_var("show tables like '$table_name'") != $table_name) {
 $sql = "CREATE TABLE $table_name (
 id INTEGER NOT NULL AUTO_INCREMENT,
 first_name TEXT NOT NULL,
 last_name TEXT NOT NULL,
 email TEXT NOT NULL,
 contact_no TEXT NOT NULL,
 subject TEXT NOT NULL,
 message TEXT NOT NULL,
 created_at datetime NOT NULL DEFAULT  CURRENT_TIMESTAMP,
 updated_at  datetime NOT NULL DEFAULT  CURRENT_TIMESTAMP,
 PRIMARY KEY (id)
 ) $charset_collate;";
 dbDelta( $sql );
}
}
register_activation_hook( __FILE__, 'jpcontact_form_create_db' );

 ?>