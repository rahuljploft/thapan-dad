
    $().ready(function() {
        $('#jpcontactform').validate({
            rules: {
                first_name: "required",
                email: "required",
                contact_no :{
        matches: "[0-9]+", 
        minlength:10,
        maxlength:10
    },
                subject : "required",
            },
            messages: {
                first_name: "Please enter first name",
                email: "Please enter valid email address",
                contact_no : "Please enter phone number",
                subject : "Please enter subject title",
    
            },
            submitHandler: function(form) {
                form.submit();
            }
    
            // any other options and/or rules
        });
    });
  